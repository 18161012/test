import mysql.connector


try:
    connection = mysql.connector.connect(host='localhost',
                                         database='ETL',
                                         user='root',
                                         password='hoang207')
    mysql_DROP_Table_Main = """Drop table if exists Main"""
    mysql_DROP_Table_weather = """Drop table if exists weather"""
    mysql_DROP_Table_sys = """Drop table if exists sys"""
    mysql_DROP_Table_location = """Drop table if exists location"""

    

    mySql_Create_Table_Weather = """CREATE TABLE Weather (
                                 WeatherID int NOT NULL PRIMARY KEY,
                                 status text ,
                                 description text ,
                                 icon varchar(10))
                                  """

    mySql_Create_Table_location = """CREATE TABLE Location (
                                  city_id int PRIMARY KEY,
                                  city_name text ,
                                  lon float NOT NULL,
                                  lat float NOT NULL,                               
                                  timezone float ,
                                  cod int                              
                                  ) """

    mySql_Create_Table_Sys = """CREATE TABLE sys (
                                  sys_id int PRIMARY KEY,
                                  type int,
                                  country_code char(10) ,                                                                  
                                  sunrise DATETIME,
                                  sunset DATETIME
                                  ) """

    mySql_Create_Table_Main = """CREATE TABLE Main (         
                             WeatherID int NOT NULL ,
                             dt DATETIME NOT NULL,
							 city_id int NOT NULL,
                             sys_id int ,
                             base text ,                             
                             temp float ,
                             feels_like float,
                             temp_min float,
                             temp_max float,
                             pressure float,
                             humidity float,
                             sea_level float,
                             grnd_level float,
                             visibility int,
                             wind_speed float,
                             wind_deg float,
                             wind_gust float,
                             clouds_all int,
                             rain_1h float,
                             rain_3h float,
                             snow_1h float,
                             snow_3h float,   
                             FOREIGN KEY (WeatherID) REFERENCES Weather(WeatherID),
							 FOREIGN KEY (city_id) REFERENCES Location(city_id),
                             FOREIGN KEY (sys_id) REFERENCES sys(sys_id),
                             PRIMARY KEY (city_id, dt, WeatherID )) """
                            

                                                         

    cursor = connection.cursor()
    result = cursor.execute(mysql_DROP_Table_Main)
    result = cursor.execute(mysql_DROP_Table_sys)
    result = cursor.execute(mysql_DROP_Table_location)
    result = cursor.execute(mysql_DROP_Table_weather)
   
    result = cursor.execute(mySql_Create_Table_Sys)
    result = cursor.execute(mySql_Create_Table_Weather)
    result = cursor.execute(mySql_Create_Table_location)
    result = cursor.execute(mySql_Create_Table_Main)
    
    print("ALL Table created successfully ")

except mysql.connector.Error as error:
    print("Failed to create table in MySQL: {}".format(error))
finally:
    if connection.is_connected():
        cursor.close()
        connection.close()
        print("MySQL connection is closed")

