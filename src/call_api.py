import requests

def call_API(city):
    url = "https://community-open-weather-map.p.rapidapi.com/weather"

    querystring = {"q": city,"lat":"0","lon":"0","lang":"null","units":"imperial","mode":"json"}

    headers = {
    "X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
    "X-RapidAPI-Key": "e7fd4bc6fbmsh1b8373d347acd16p1e5303jsn5dfecc6cc7f5"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    return response.json()