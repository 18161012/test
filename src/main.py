from bdb import effective
import json
#from termios import VINTR
from datetime import datetime, timedelta
import mysql.connector
import requests
import json

#thanhpho = str(input("choose the city: "))
url = "https://community-open-weather-map.p.rapidapi.com/weather"

querystring = {"q":"london","lat":"0","lon":"0","id":"2172797","lang":"null","units":"imperial"}

headers = {
	"X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
	"X-RapidAPI-Key": "136b82b866msh5bca7a447715978p183d6ejsn1c06701b77a9"
}

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)

with open('data.json', "w") as file_write:
# write json data into file
 	json.dump(response.json(), file_write)
connection = mysql.connector.connect(host='localhost',
                                         database='ETL',
                                         user='root',
                                         password='hoang207')
  
# Opening JSON file
f = open('data.json')
  
# returns JSON object as 
# a dictionary
data = json.load(f)

def coord(coord):
    lon = coord.get('lon')
    lat = coord.get('lat')
    return lon, lat
#2. get the status of weather
def weather(weather):
    id = weather.get('id')
    main = weather.get('main', None)
    description = weather.get('description', None)
    icon = weather.get('icon', None)
    return id, main, description, icon
#3. get the environment parameters
def main(main):
    temp = main.get('temp', None)
    feels_like = main.get('feels_like', None)
    temp_min = main.get('temp_min', None)
    temp_max = main.get('temp_max', None)
    pressure = main.get('pressure', None)
    humidity = main.get('humidity', None)
    sea_level = main.get('sea_level', None)
    grnd_level = main.get('grnd_level', None)
    
    return temp, feels_like, temp_min, temp_max, pressure, humidity, sea_level, grnd_level
#4. get the information when having rain or not
def No_rain(rain):
    rain['rain'] = {}
    rain['rain']['1h'] = None
    rain['rain']['3h'] = None
    rain_1h = rain['rain']['1h']
    rain_3h = rain['rain']['3h']
    return rain_1h, rain_3h
def Yes_rain(rain):
    rain_1h = rain['rain'].get('1h', None)
    rain_3h = rain['rain'].get('3h', None)
    return rain_1h, rain_3h
#5. get the information when having snow or not
def No_snow(snow):
    snow['snow'] = {}
    snow['snow']['1h'] = None
    snow['snow']['3h'] = None
    snow_1h = snow['snow']['1h']
    snow_3h = snow['snow']['3h']
    return snow_1h, snow_3h   
def Yes_snow(snow):
    snow_1h = snow['snow'].get('1h', None)
    snow_3h = snow['snow'].get('3h', None)
    return snow_1h, snow_3h
#6. get the information about wind
def wind(wind):
    speed = wind.get('speed', None)
    deg = wind.get('deg', None)
    gust = wind.get('gust', None)
    return speed, deg, gust
#7. get the information about cloud
def clouds(clouds):
    all = clouds.get('all', None)
    return all
#8. get the data about dt and convert it to date and time
def dt(dt, timezone):
    time = datetime.utcfromtimestamp(int(dt)).strftime('%Y-%m-%d %H:%M:%S') # convert dt into date and time, type: str
    timezone1 = timezone    # Timezone
    time = datetime.fromisoformat(time)     # change type from str to datetime
    time = time + timedelta(hours=(timezone1/3600))     # Plus the timezone to find correct time
    return time
#9. get the data about sys
def sys(coord, timezone):
    sys_id = coord['id']
    type = coord['type']    
    country_code = coord['country']
    timezone1 = timezone

    
    time_sunrise = datetime.utcfromtimestamp(int(coord['sunrise'])).strftime('%Y-%m-%d %H:%M:%S')
    time_sunrise= datetime.fromisoformat(time_sunrise)
    sunrise = time_sunrise + timedelta(hours=(timezone1/3600))
    
    time_sunset = datetime.utcfromtimestamp(int(coord['sunset'])).strftime('%Y-%m-%d %H:%M:%S')
    time_sunset= datetime.fromisoformat(time_sunset)
    sunset = time_sunset + timedelta(hours=int(timezone1/3600))
    
    return sys_id, type, country_code, sunrise,sunset


mySql_insert_sys = """INSERT INTO sys (sys_id, type, country_code, sunrise, sunset) 
                                VALUES (%s, %s, %s, %s, %s) """

sys_id, type, country_code, sunrise, sunset = sys(data['sys'], data['timezone'])
record = (sys_id, type, country_code,  sunrise, sunset)
cursor = connection.cursor()
cursor.execute(mySql_insert_sys, record)
connection.commit()
print("Record inserted successfully into sys table")

mySql_insert_location = """INSERT INTO Location (city_id, city_name, lon, lat, timezone, cod) 
                                VALUES (%s, %s, %s, %s, %s, %s) """

city_id = data['id']      
city_name = data['name'] 
lon, lat = coord(data['coord'])        
timezone = data['timezone']
cod = data['cod']
record = (city_id, city_name, lon, lat, timezone, cod)
cursor = connection.cursor()
cursor.execute(mySql_insert_location, record)
connection.commit()
print("Record inserted successfully into Location table")

mySql_insert_weather = """INSERT INTO Weather (weatherID, status, description, icon) 
                                VALUES (%s, %s, %s, %s) """

id, status, description, icon = weather(data['weather'][0])
record = (id, status, description, icon)
cursor.execute(mySql_insert_weather, record)
connection.commit()
print("Record inserted successfully into Weather table")

mySql_insert_Main = """INSERT INTO Main (WeatherID,  dt, city_id, sys_id, base, temp, feels_like, temp_min, temp_max, pressure, humidity, sea_level, grnd_level, visibility, wind_speed, wind_deg, wind_gust, clouds_all, rain_1h, rain_3h, snow_1h, snow_3h) 
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """

weather_id, main_ID, description, icon = weather(data['weather'][0])
time = dt(data['dt'], data['timezone'])
city_id = data['id']
sys_id, type, country_code, sunrise, sunset = sys(data['sys'], data['timezone'])      
base = data['base']
temp, feels_like, temp_min, temp_max, pressure, humidity, sealevel, grndlevel = main(data['main'])
visibility = data['visibility']
speed, deg, gust = wind(data['wind'])
cloud = clouds(data['clouds'])
        
if 'rain' not in data.keys():
            rain_1h, rain_3h = No_rain(data)
else:
            rain_1h, rain_3h = Yes_rain(data)
        
if 'snow' not in data.keys():
            snow_1h, snow_3h = No_snow(data)
else:
            snow_1h, snow_3h = Yes_snow(data)
            
record = (weather_id, time, city_id, sys_id, base, temp, feels_like, temp_min, temp_max, pressure, humidity, sealevel, grndlevel, visibility, speed, deg, gust, cloud, rain_1h, rain_3h, snow_1h, snow_3h)
cursor.execute(mySql_insert_Main, record)
connection.commit()
print("Record inserted successfully into Main table")

# Closing file
f.close()

